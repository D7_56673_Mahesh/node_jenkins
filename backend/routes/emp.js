const { request, response } = require('express')
const express=require('express')

const router=express.Router()

const db =require('../db')
const utils =require('../utils')

router.get('/all',(request,response)=>{
    const connection= db.openConnection()

    const sql=`select * from Emp `

    connection.query(sql,(error,data)=>{
        response.send(utils.createResult(error,data))
    })
})

router.post('/add',(request,response)=>{
    const {name,salary,age}=request.body
    const sql=`insert into Emp (name,salary,age) values ('${name}','${salary}','${age}')`;

    const connection=db.openConnection()
    connection.query(sql,(error,data)=>{

        response.send(utils.createResult(error,data))
    })
})
router.patch('/edit/:id',(request,response)=>{
    const connection=db.openConnection()
    const{salary}=request.body
    const{id}=request.params
    const sql= `update Emp set 
    salary='${salary}'
    where
    empid='${id}'`
    connection.query(sql,(error,data)=>{

        response.send(utils.createResult(error,data))
    })
})

router.delete('/delete/:id',(request,response)=>{
    const connection=db.openConnection()
    const{id}=request.params
    const sql= `delete from Emp
    where
    empid='${id}'`
    connection.query(sql,(error,data)=>{

        response.send(utils.createResult(error,data))
    })
})

module.exports=router